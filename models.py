import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship
from sqlalchemy import (
    text, Column, Integer, String, Date,
    DateTime, ForeignKey, UniqueConstraint, CheckConstraint,
)

# Base = declarative_base() # old way
class Base(DeclarativeBase):    # new way
    pass

# create table students(id int not null auto_increment primary key,
# first_name varchar(64) not null,
# last_name varchar(64) not null,
# birthday date null
class Student(Base):
    __tablename__ = 'students'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    # id: Mapped[int] = mapped_column(autoincrement=True, primary_key=True, nullable=False)
    first_name = Column(String(64), nullable=False)
    # first_name: Mapped[str] = mapped_column(64, nullable=False)
    last_name = Column(String(64), nullable=False)
    birthday = Column(Date, nullable=True)

    locker = relationship('Locker', back_populates='student', uselist=False)
    address = relationship('Address', back_populates='student', uselist=False)
    grades = relationship("Grade", back_populates="student")

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def __repr__(self):
        return f'Student(id={self.id}, first_name={self.first_name!r}, last_name={self.last_name!r})'


class Locker(Base):
    __tablename__ = 'lockers'
    __table_args__ = (
        UniqueConstraint('number', name="uix_lockers_number"),
    )

    student_id = Column(Integer, ForeignKey('students.id'), primary_key=True, nullable=False)
    number = Column(String(4), nullable=False)

    student = relationship(Student, back_populates='locker')

    def __str__(self):
        return self.number


class Address(Base):
    __tablename__ = "addresses"

    student_id = Column(Integer, ForeignKey("students.id"), primary_key=True, nullable=False)
    street_name = Column(String(128), nullable=False)
    number = Column(Integer, nullable=True)
    city = Column(String(64), nullable=False)

    student = relationship(Student, back_populates="address")

    def __str__(self):
        return f'{self.street_name}, {self.number}, {self.city}'

    def __repr__(self):
        return f'Address(student_id={self.student_id}), address={self}'


class Grade(Base):
    __tablename__ = "grades"
    __table_args__ = (
        CheckConstraint(text("grade >= 4 AND grade <= 10")),
    )

    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True)
    student_id = Column(Integer, ForeignKey(Student.id), nullable=False)
    grade = Column(Integer, nullable=False)
    created = Column(DateTime, default=datetime.datetime.utcnow)

    student = relationship(Student, back_populates="grades")

    def __str__(self):
        return f'{self.student_id} -> {self.grade}'
