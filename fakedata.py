import sys

from faker import Faker
from sqlalchemy import create_engine, text

from config import DB_URL
from db import engine, Session
from models import Base, Student, Locker, Address


def create_schema():
    eng = create_engine(DB_URL.removesuffix("students"))
    conn = eng.connect()
    conn.execute(text("CREATE SCHEMA IF NOT EXISTS students"))


def main(argv: list[str] | None = None) -> None:
    if argv is None:
        argv = sys.argv[1:]

    create_schema()
    Base.metadata.create_all(bind=engine)

    fake = Faker()

    session = Session()

    for i in range(1, 101):
        student = Student(
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            birthday=fake.date_between('-18y', '-14y'),
        )
        locker = Locker(student=student, number=f'{i:04}')
        address = Address(
            student=student,
            street_name=fake.street_name(),
            number=i,
            city=fake.city(),
        )
        session.add(student)
        session.add(locker)
        session.add(address)

    session.commit()



if __name__ == '__main__':
    main()
